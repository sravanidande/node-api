const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoose = require('mongoose');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect('mongodb://localhost/nodeApi', { useNewUrlParser: true });

const personModel = mongoose.model('person', {
  firstname: String,
  lastname: String
});

// app.use('/', (req, res) => {
//   res.send('welcome to node api');
// });

app.post('/person', async (req, res, next) => {
  try {
    const person = new personModel(req.body);
    const result = await person.save();
    res.send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get('/person', async (req, res, next) => {
  try {
    const result = await personModel.find().exec();
    res.send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get('/person/:id', async (req, res, next) => {
  try {
    const person = await personModel.findById(req.params.id).exec();
    res.send(person);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.put('/person/:id', async (req, res, next) => {
  try {
    const person = await personModel.findById(req.params.id).exec();
    person.set(req.body);
    const result = await person.save();
    res.send(result);
  } catch (err) {
    res.status(500).send(err);
  }
});

// app.delete('/person/:id', async (req, res, next) => {
//   try {
//     const person = await personModel.findById(req.params.id).exec();
//     person.delete();
//     const result = await person.save();
//     res.send(result);
//   } catch (err) {
//     res.status(500).send(err);
//   }
// });

app.delete('/person/:id', async (req, res, next) => {
  try {
    const person = await personModel.deleteOne({ _id: req.params.id }).exec();
    res.send(person);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.listen(3000, () => console.log('app lsitening on port 3000....'));
